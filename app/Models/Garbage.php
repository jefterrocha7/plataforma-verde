<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Garbage extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'type',
        'category',
        'treatment_tech',
        'class',
        'measure_unit',
        'weight'
    ];

    public function getWeightAttribute() {
        return $this->attributes['weight'] / 100;
    }

    public function setWeightAttribute($attr) {
        return $this->attributes['weight'] = is_float($attr) ?
            $attr * 100 : $attr;
    }
}
