<?php

namespace App\Imports;

use App\Models\Garbage;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\RemembersRowNumber;

class GarbageImport implements ToModel
{
    use RemembersRowNumber;

    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
        $currentRowNumber = $this->getRowNumber();
        $filtered_row = array_filter($row);
        if ($currentRowNumber > 5)
            return new Garbage([
                'name' => $filtered_row[1],
                'type' => $filtered_row[2],
                'category' => $filtered_row[3],
                'treatment_tech' => $filtered_row[4],
                'class' => $filtered_row[5],
                'measure_unit' => $filtered_row[6],
                'weight' => $filtered_row[7]
            ]);
    }
}
