<?php

namespace App\Jobs;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\GarbageImport;

class ProcessGarbageSpreedSheet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $spreadsheet;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($spreadsheet)
    {
        $this->spreadsheet = $spreadsheet;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        sleep(10);
        if ($this->spreadsheet)
            Excel::import(new GarbageImport, $this->spreadsheet);
    }
}
