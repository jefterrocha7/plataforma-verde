<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jobs\ProcessGarbageSpreedSheet;
use App\Models\Garbage;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\GarbageImport;


class GarbageController extends Controller
{
    private $garbage;

    public function __construnct(Garbage $garbage)
    {
        $this->garbage = $garbage;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Garbage::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        // Excel::import(new GarbageImport, $request->spreadsheet);
        $path1 = $request->spreadsheet->store('temp');
        // $path = storage_path('app').'/'.$path1;

        // Excel::import(new GarbageImport, $path1);
        // dd($path);
        $aa = ProcessGarbageSpreedSheet::dispatch($path1);
        return response()->json(['success' => true ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Garbage  $garbage
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $garbage = Garbage::find($id);
        if (is_null($garbage))
            return response()->json([
                'message' => 'not found'
            ], 404);
        return $garbage;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Garbage  $garbage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $garbage = Garbage::find($id);
        if (is_null($garbage))
            return response()->json([
                'message' => 'not found'
            ], 404);
        // dd($request->all());
        $garbage->update($request->all());
        return response()->json(['message' => 'successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Garbage  $garbage
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $garbage = Garbage::find($id);
        if (is_null($garbage))
            return response()->json([
                'message' => 'not found'
            ], 404);

        $garbage->delete();
        return response()->json(['message' => 'successfully deleted']);
    }
}
